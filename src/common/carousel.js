var widthClassOptions = ({
    bestseller: 'bestseller_default_width',
    featured: 'featured_default_width',
    special: 'special_default_width',
    latest: 'latest_default_width',
    related: 'related_default_width',
    additional: 'additional_default_width',
    tabbestseller: 'tabbestseller_default_width',
    tabfeatured: 'tabfeatured_default_width',
    tabspecial: 'tabspecial_default_width',
    tablatest: 'tablatest_default_width',
    module: 'module_default_width',
    testimonial: 'testimonial_default_width',
    testcms: 'testcms_default_width'
});

const productCarouselAutoSet = (id) => {
    var object = $('#' + id);
    var objectID = object.attr('id');
    var myObject = objectID.replace('-carousel', '');
    if (myObject.indexOf("-") >= 0)
        myObject = myObject.substring(0, myObject.indexOf("-"));
    if (widthClassOptions[myObject])
        var myDefClass = widthClassOptions[myObject];
    else
        var myDefClass = 'grid_default_width';
    object.sliderCarousel({
        defWidthClss: myDefClass,
        subElement: '.slider-item',
        subClass: 'product-block',
        firstClass: 'first_item_tm',
        lastClass: 'last_item_tm',
        slideSpeed: 200,
        paginationSpeed: 800,
        autoPlay: false,
        stopOnHover: false,
        goToFirst: true,
        goToFirstSpeed: 1000,
        goToFirstNav: true,
        pagination: true,
        paginationNumbers: false,
        responsive: true,
        responsiveRefreshRate: 200,
        baseClass: "slider-carousel",
        theme: "slider-theme",
        autoHeight: true
    });

    var nextButton = object.parent().find('.next');
    var prevButton = object.parent().find('.prev');
    nextButton.click(function () {
        object.trigger('slider.next');
    })
    prevButton.click(function () {
        object.trigger('slider.prev');
    })
}

export default productCarouselAutoSet