const Notifications = {
    state: [], // here the notifications will be added

    add(notification) {
        this.state.push(notification)
    },
    remove(notification) {
        var index = this.state.indexOf(notification)
        this.state.splice(index, 1)
    }
}

export default Notifications