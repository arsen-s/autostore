import Vue from 'vue'
import VueTranslate from 'vue-translate-plugin'
import SidebarLeft from './components/sidebar/index.vue'
import Notifications from './components/notifications/index.vue'
import Breadcrumbs from './components/breadcrumbs/index.vue'
import Loading from './components/loading/loading.vue'
import VueMoment from 'vue-moment'
import ProductMixin from './mixins/product.vue'
import CartMixin from './mixins/cart.vue'
import HeadMixin from './mixins/head.vue'
import axios from 'axios'
import uuid from 'uuid/v4'
import productCarouselAutoSet from './common/carousel'
import VueHead from 'vue-head'
import VueBreadcrumbs from 'vue2-breadcrumbs'
import NProgress from 'nprogress'
import VueScrollTo from 'vue-scrollto'

import store from './store'
import router from './router'
import config from '../config'
import bus from './bus'



Vue.use(VueHead,  {
    separator: '-',
    complement: 'Доставка товарів з Польщі'
})
Vue.use(VueTranslate)
Vue.use(VueMoment)
Vue.use(VueBreadcrumbs)


if (localStorage.getItem('token') === null) {
    localStorage.setItem('token', uuid())
}

axios.defaults.baseURL = process.env.NODE_ENV === 'production'
    ? config.build.baseUrl
    : config.dev.baseUrl


axios.interceptors.request.use(config => {
    NProgress.start();
    return config;
}, error => {
    return Promise.reject(error);
});

axios.interceptors.response.use(response => {
    NProgress.done();

    if (response.status == 404) {
        router.go('/');
    }
    return response;
}, error => {
    return Promise.reject(error);
});


Vue.prototype.$http = axios

window.$ = window.jQuery = require('assets/js/jquery/jquery-2.1.1.min.js')
window.Vue = Vue
window.bus = bus
window.uuid = uuid
window.ScrollTo = VueScrollTo
window.productCarouselAutoSet = productCarouselAutoSet
window.loading = function (show) {
    show ? $('.loading').fadeIn() : $('.loading').fadeOut(1000);
}

Vue.component('sidebar-left', SidebarLeft)
Vue.component('notifications', Notifications)
Vue.component('breadcrumbs', Breadcrumbs)
Vue.component('loading', Loading)

Vue.mixin(ProductMixin)
Vue.mixin(CartMixin)
Vue.mixin(HeadMixin)


require('assets/js/megnor/jquery.custom.min.js')
require('assets/js/megnor/custom.js')
require('assets/js/megnor/megnor.min.js')
require('assets/js/megnor/jstree.min.js')
require('assets/js/jquery/magnific/jquery.magnific-popup.min.js')
/* require('assets/js/bootstrap/js/bootstrap.min.js')
 require('assets/js/common.js')
 require('assets/js/megnor/scrolltop.min.js')
 require('assets/js/megnor/jquery.formalize.min.js')
 require('assets/js/megnor/tabs.js')*/
require('assets/js/megnor/jstree.min.js')
require('assets/js/jquery/owl-carousel/owl.carousel.min.js')
require('assets/js/megnor/carousel.min.js')

// ======================= Base Component ======================
import App from './App'

console.log(Vue.translate)
// ======================== Vue Instance =======================
/* eslint-disable no-new */
new Vue({
    router,
    store,
    el: '#app',
    render: h => h(App)
})
