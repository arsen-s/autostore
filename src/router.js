import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

// ===================== Pages Components ======================
import Home from './components/home/index.vue'
import Page from './components/page/index.vue'

// ==================== Router registration ====================
export default new Router({
    mode: 'hash',
    routes: [
        {path: '/', name: 'home', component: Home, title: 'Головна'},
        {path: '/cart', name: 'cart', component: Page, meta: {title: 'Корзина'}},
        {path: '/contacts', name: 'contacts', component: Page, meta: {title: 'Контакти'}},
        {path: '/about-us', name: 'about-us', component: Page, meta: {title: 'Про нас'}},
        {path: '/reviews', name: 'reviews', component: Page, meta: {title: 'Відугки'}},
        {path: '/catalog', name: 'catalog', component: Page, meta: {title: 'Каталог'}},
        {path: '/categories/:id/products', name: 'products', component: Page, meta: {title: 'Каталог'}},
        {path: '/products/:id', name: 'product', component: Page, meta: {title: 'Описание товара'}},
        {path: '*', name: '404', component: {'template': '404! Page not found'}, title: '404'},
    ]
})
